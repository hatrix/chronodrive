from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pyvirtualdisplay import Display
from selenium import webdriver
import time

import config

class Chrono:
    def __init__(self):
        self.session = webdriver.Firefox()


    def wait_for(self, by, el_name, timeout=20):
        el = WebDriverWait(self.session, timeout).until(
                EC.presence_of_element_located((by, el_name))
        )
        return el


    def wait_for_page_to_load(self, timeout=20):
        w = WebDriverWait(self.session, timeout)
        w.until(lambda browser: browser.execute_script("return document.readyState") == "complete")


    def choose_location(self, postal_code):
        url = 'https://www.chronodrive.com/prehome'

        code_elm = '//*[@id="searchField"]'
        btn_elm = '//*[@id="linksubmit"]'
        wait_elm = '//*[@id="resultZone"]'

        self.session.get(url)
        self.wait_for_page_to_load()

        code_input = self.session.find_element_by_xpath(code_elm)
        btn_input = self.session.find_element_by_xpath(btn_elm)
        
        code_input.send_keys(postal_code)
        btn_input.click()

        store_elm = '/html/body/div[3]/div[2]/div/ul/li[1]/div[2]/a[2]'
        self.wait_for(By.XPATH, store_elm)

        store_input = self.session.find_element_by_xpath(store_elm)
        store_input.click()


    def login(self, user, passwd):
        user_elm = '//*[@id="email_login"]'
        passwd_elm = '//*[@id="pwd_login"]'
        btn_elm = '//*[@id="loginForm"]/button'
        wait_elm = '//*[@id="baccount"]/div/p'

        url = 'https://www.chronodrive.com/login'

        self.session.get(url)
        self.wait_for_page_to_load()

        user_input = self.session.find_element_by_xpath(user_elm)
        passwd_input = self.session.find_element_by_xpath(passwd_elm)
        btn_input = self.session.find_element_by_xpath(btn_elm)

        user_input.send_keys(user)
        passwd_input.send_keys(passwd)
        btn_input.click()

        self.wait_for(By.XPATH, wait_elm)


    def get_slots(self):
        slots = '//*[@id="m_panier"]/div[4]'

        self.wait_for(By.XPATH, slots)
        time.sleep(1)
        slots = self.session.find_element_by_xpath(slots)

        return slots.text


if __name__ == '__main__':
    display = Display(visible=0, size=(800, 600))
    display.start()

    chrono = Chrono()
    chrono.choose_location(config.postal_code)
    chrono.login(config.email, config.password)
    slots = chrono.get_slots()

    print(slots)

    display.stop()
