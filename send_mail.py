import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header

from chronodrive import Chrono
import config

def send_emails(receivers, subject, body):
    # Setup SMTP
    port = config.smtp_port
    smtp_server = config.smtp_server
    #password = getpass.getpass('Password: ')
    password = config.password_email

    # Setup message and subject to be sent
    msg = MIMEMultipart('alternative')
    msg.set_charset('utf8')
    msg['FROM'] = config.sender_email
    msg['Subject'] = subject
    _attach = MIMEText(body.encode('utf-8'), 'plain', 'UTF-8')
    msg.attach(_attach)

    # Connect to the server
    context = ssl.create_default_context()
    server = smtplib.SMTP(smtp_server,port)
    server.starttls(context=context) # Secure the connection
    server.login(msg['FROM'], password)
        
    # Send a message to every receiver
    msg['To'] = ', '.join(receivers)
    server.sendmail(msg['FROM'], receivers, msg.as_string())


if __name__ == "__main__":
    receivers = [config.sender_email]

    from pyvirtualdisplay import Display
    display = Display(visible=0, size=(800, 600))
    display.start()

    chrono = Chrono()
    chrono.choose_location(config.postal_code)
    chrono.login(config.email, config.password)
    slots = chrono.get_slots()
    display.stop()

    print(slots)
    #if slots != 'Pas de créneaux disponibles':  
    send_emails(receivers, subject="Chronodrive créneaux", body=slots)
